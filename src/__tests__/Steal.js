import React from 'react';
import { shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';
import Steal from '../components/Steal';

configure({adapter: new Adapter()});

const conF = {
    users: [
        {
          "username": "eva01",
          "colour": "green",
          "settlements": [],
          "cities": [],
          "roads": [],
          "development_cards": 0,
          "resources_cards": 0,
          "victory_points": 0,
          "last_gained": []
        },
        {
          "username": "eva02",
          "colour": "blue",
          "settlements": [],
          "cities": [],
          "roads": [],
          "development_cards": 0,
          "resources_cards": 0,
          "victory_points": 0,
          "last_gained": []
        }
      ]
};

describe('Steal testing', () => {
    describe('Contains form', () => {
        const comm = shallow(<Steal {...conF}/>);
        it('Contains a single form', () => {
            expect(comm.find('Form').length).toEqual(1)
        });
    });
});