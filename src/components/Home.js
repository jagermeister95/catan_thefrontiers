import React from 'react'

function Home() {
    return (
    	<div className="tc">
    		<h1 className="dark-gray">Los Colonos de Catan</h1>
    		<img src="/img/colonos.jpg"
            className="container" 
            alt="Site Name"/>
    	</div>
    );
}

export default Home