import React from 'react'

function NotFound() {
    return (
        <div className="tc">
            <h1>404</h1>
            <h2>Página no encontrada!</h2>
            <img src="/img/404.jpg" alt="Not in our archives"/>
        </div>
    )
}

export default NotFound